import setBackgroundClass from "./setBackgroundClass";
import setForecastEndpoint from "./setForecastEndpoint";

export { setBackgroundClass, setForecastEndpoint };
